# Hey there :wave:
[![Mastodon](https://img.shields.io/badge/Mastodon-blue?style=flat-square&logo=mastodon&labelColor=rgb(0%2C%200%2C%200)&color=%235b63b7)](https://infosec.exchange/@ximnoise)
[![Blog](https://img.shields.io/website?url=https%3A%2F%2Fblog.maximilianrogge.com%2F&style=flat-square&label=Blog&labelColor=%23000000&color=%235b63b7)](https://blog.maximilianrogge.com)

I'm Maximilian Rogge aka Ximnoise, a passionate self-taught junior-sysadmin and full-stack webdeveloper from germany with passion for cybersecurity. I am also an open-source enthusiast and i love how collaboration and knowledge sharing happened through open-source.

You can see my work on my [Portfolio](https://maximilianrogge.com).

```javascript
const ximnoise = {
    pronouns: ["He", "Him"].
    code: ["Javascript", "Typescript", "HTML", "CSS", "Golang", "Rust", "Python"],
    askMeAbout: ["web dev", "cybersecurity", "linux", "keyboards"],
    technologies: {
        backEnd: {
            js: ["Node", "Express"],
        },
        devOps: ["Docker", "Nginx", "Ansible", "CI/CD Pipeline"],
        databases: ["MongoDB", "Firebase", "MySQL", "MariaDB", "PostgreSQL"],
        misc: ["Postman", "Bash", "Heroku", "Netlify", "Hugo", "Git"]
    },
    architecture: ["Serverless Architecture", "Progressive web applications", "Single page applications", "Container Orchestration"],
    currentFocus: "Learning SOC Analyst, Pentesting and Python",
};
```
![Image](https://media.giphy.com/media/13HgwGsXF0aiGY/giphy.gif)


